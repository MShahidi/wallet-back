package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.Review;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReviewRepository extends CrudRepository<Review, String> {
    List<Review> findByProfileId(UUID profileId);
}
