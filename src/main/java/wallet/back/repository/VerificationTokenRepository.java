package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.VerificationToken;

import java.util.Optional;

@Repository
public interface VerificationTokenRepository extends CrudRepository<VerificationToken, String> {
    Optional<VerificationToken> findByEmail(String email);

    Optional<VerificationToken> findByToken(String token);
}
