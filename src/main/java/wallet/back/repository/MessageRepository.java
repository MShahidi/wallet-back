package wallet.back.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import wallet.back.domain.entity.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, String> {
}
