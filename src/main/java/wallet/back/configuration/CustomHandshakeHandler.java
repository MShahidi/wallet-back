package wallet.back.configuration;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public class CustomHandshakeHandler extends DefaultHandshakeHandler {

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        System.out.println("---- NEW USER -----");
        if (request.getPrincipal() != null)
            System.out.println(request.getPrincipal().getName());
        else
            System.out.println("Request principal is null --------");
        String s = UUID.randomUUID().toString();
        return new StompPrincipal(s);
    }
}
