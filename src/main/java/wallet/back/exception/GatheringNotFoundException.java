package wallet.back.exception;

public class GatheringNotFoundException extends Exception {
    public GatheringNotFoundException() {
        super("Gathering not found");
    }
}
