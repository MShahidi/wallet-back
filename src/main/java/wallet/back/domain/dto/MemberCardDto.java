package wallet.back.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import wallet.back.domain.entity.Profile;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MemberCardDto implements IOutputDto<Profile> {
    String firstName;
    String lastName;
    String avatar;
    String username;


    @Override
    public IOutputDto<Profile> toDto(Profile profile) {
        return MemberCardDto.builder()
                .firstName(profile.getFirstName())
                .lastName(profile.getLastName())
                .avatar(profile.getAvatar())
                .username(profile.getUser().getUsername())
                .build();
    }
}
