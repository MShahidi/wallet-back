package wallet.back.domain.dto;

import lombok.Data;
import wallet.back.domain.entity.User;

@Data
public class SignupDto implements IInputDto<User> {

    String email;
    String password;

    @Override
    public boolean isValid() {
        return email != null && !email.isEmpty() && email.contains("@") && password != null && !password.isEmpty();
    }

    @Override
    public User fromDto() {
        return User.builder()
                .email(this.email)
                .password(this.password)
                .active(false)
                .build();
    }

    @Override
    public User updateDto(User user) {
        return user.toBuilder().email(this.email).password(this.password).build();
    }
}
